#!/bin/bash
#
# Extract results from the maillist data
# for a given gcc version and date range
#

base=$HOME/projects/gcc-buildstat-tools
htdocsbase=$HOME/projects/gcc/wwwdocs/htdocs
baseurl=https://gcc.gnu.org/ml/gcc-testresults
gtsout=$(mktemp)
# By default try and cvs update the buildstat.html file
skipcvs=0

# Cleanup
trap 'rm -f $gtsout' EXIT

print_usage()
{
  echo "Usage: $(basename $0) -g <gccver> -m <months> [ -h <htmlsummary> ] [ -o <summary> ] [ -s ]"
  echo
  echo "-g gcc major.minor version to find results for"
  echo "-m how many months in the past to look for results"
  echo "-h output html summary to this file"
  echo "-o output summary to this file instead of stdout"
  echo "-s skip doing cvs update of buildstat.html"
  echo
}

do_cvs_update()
{
  # Setup pointer to upstream buildstat.html
  docdir=$htdocsbase/gcc-$htdocsver
  # and do a cvs update to make sure we have the latest
  set -e
  pushd $htdocsbase >/dev/null 2>&1
  cvs -q update >/dev/null 2>&1
  set +e
  #cvs diff buildstat.html
  popd >/dev/null 2>&1
}

create_summary()
{
  # Find all the months we've been asked to do
  # and run through them
  for ((d=0;d<=$months;d++));
  do
    extraarg=
    date=$(date -d "$d months ago" +%Y-%m)
    # Check if file exists and then make curl use its timestamp
    # to determine if the file should be fetched
    [ -f data/gts$date ] && extraarg="-z data/gts$date"
    # Fetch upstream testresults from maillist index
    curl $extraarg --retry 2 -L -S -s -o data/gts$date ${baseurl}/$date/

  # Parse testresults from webindex
  ./gcc-testresult-summary data/gts$date $gccver >> $gtsout
  done
  # Output
  if [ -n "$summaryfile" ]; then
    cp $gtsout $summaryfile
  else
    cat $gtsout
  fi
}

create_html_summary()
{
  # Initialize html output file
  cat <<EOF > $htmlsummaryfile
<html>
<head>
<title>buildstat status for gcc $gccver created on $(date +%Y-%m-%d)"</title>
</head>
<body>
EOF
  # Output summary
  IFS="#"
  while read version platform url
  do
    echo "<p>Missing: $version#$platform#<a href="$url">$url</a></p>" >> $htmlsummaryfile
  done < $gtsout
  unset IFS
  # Close html output file
  cat <<EOF >> $htmlsummaryfile
</body>
</html>
EOF
}

# Parse options
if [ $# -gt 0 ]; then
  while getopts g:m:h:o:s opt
  do
    case $opt in
      g)
	gccver=$OPTARG
	major=$(echo $gccver | cut -d. -f1)
	if [ $major -ge 5 ]; then
	  htdocsver=$major
	else
	  htdocsver=$gccver
	fi
	;;
      m)
	months=$OPTARG
	;;
      h)
	htmlsummaryfile=$OPTARG
	;;
      o)
	summaryfile=$OPTARG
	;;
      s)
	skipcvs=1
	;;
      \?)
	print_usage && exit 0
	;;
    esac
  done
  shift `expr $OPTIND - 1`
else
  print_usage && exit 0
fi

# Check arguments
[ -z "$gccver" ] && print_usage && exit 1
[ -z "$months" ] && print_usage && exit 2

# let's go!

# Work from here
cd $base
[ $skipcvs -eq 0 ] && do_cvs_update
create_summary
[ -n "$htmlsummaryfile" ] && create_html_summary
