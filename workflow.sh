#!/bin/bash
#
# Run the buildstat update workflow
#
# 1. Get the summary
# 2. Review the testresults and pick the ones to add
# 3. Update buildstat.html
# 4. Construct email with mutt

toolbase=$HOME/projects/gcc-buildstat-tools
gtsout=$(mktemp)
bsnew=$(mktemp)
mailbody=$(mktemp)
# Do review by default
skipreview=0
# By default try and cvs update the buildstat.html file
# that we've been pointed at
skipcvs=0
# Where to mail the patch
mail_recip=gcc-patches@gcc.gnu.org

trap 'rm -f $gtsout $bsnew $mailbody' EXIT INT

print_usage()
{
  echo "Usage: $(basename $0) -g <gccver,gccver,...> -m <months> -b <buildstat.html> [ -s ] [ -c ]"
  echo
  echo "-g list of gcc major.minor versions to work on"
  echo "-m months"
  echo "-b buildstat.html file from the gcc website"
  echo "-s skip review"
  echo "-c skip cvs update of buildstat.html"
  echo
}

get_summary()
{
  local version temp uout r line
  temp=$(mktemp)
  for version in $(echo $gccver | tr ',' ' ')
  do
    ./gcc-summary.sh -g $version -m $months -o $gtsout -s
    cat $gtsout >> $temp
  done
  # Trim already registered results out of the summary
  ./gcc-updater.sh -b $bssrc -r $temp -o $bsnew -m $mailbody -s -d |
  while read line
  do
    case $line in
	WARNING:*)
	    r=$(echo "$line" | sed -n 's/WARNING: \([^ ]*\) already.*/\1/p')
	    sed -i "/$r/d" $temp
	    ;;
	ERROR:*)
	    echo "$line"
	    ;;
    esac
  done
  mv $temp $gtsout
}

# For each result launch elinks to review
# then ask if the result should be removed
# on exit
# FIXME: reviewing 15 near identical results one at a time suck!
do_review()
{
  for url in $(cat $gtsout | cut -d# -f3)
  do
    xterm -geometry 100x52+60+0 -e elinks $url
    echo -n "Delete $url from summary? "
    read answer
    case $answer in
      y|Y)
	sed -i "s@$url@d" $gtsout
	;;
    esac
  done
}

# Now update buildstat.html
update_buildstat()
{
  ./gcc-updater.sh -b $bssrc -r $gtsout -o $bsnew -m $mailbody -s
  diff -u $bssrc $bsnew > /tmp/buildstat-${majorver}.patch
  mutt -s "[wwwdocs] Buildstat update for $xver" -i $mailbody -a /tmp/buildstat-${majorver}.patch -- $mail_recip
}

# Parse options
if [ $# -gt 0 ]; then
  while getopts g:m:b:sc opt
  do
    case $opt in
      g)
	gccver=$OPTARG
	# Calculate versions
	majorver=${gccver%%.*}
	xver="${majorver}.x"
	;;
      m)
	months=$OPTARG
	;;
      b)
	bssrc=$OPTARG
	;;
      s)
	skipreview=1
	;;
      c)
	skipcvs=1
	;;
      \?)
	print_usage && exit 0
	;;
    esac
  done
  shift `expr $OPTIND - 1`
else
  print_usage && exit 0
fi

# Check arguments
[ -z "$gccver" ] && print_usage && exit 1
[ -z "$months" ] && print_usage && exit 2
[ ! -r "$bssrc" ] && print_usage && exit 3

# let's go!

# Normally gcc-summary.sh does cvs update but to avoid excessive runs we make
# it skip the update and instead do it there.
if [ $skipcvs -eq 0 ]; then
  pushd $(dirname $bssrc) 2>/dev/null
  cvs -q update
  popd 2>/dev/null
fi
get_summary
if [ $(wc -l < $gtsout) -gt 0 ]; then
  [ $skipreview -eq 0 ] && do_review
  update_buildstat
else
  echo "No testresults found!"
fi
