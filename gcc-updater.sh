#!/bin/bash

# Read a buildstat.html file and parse it into datastructures
#

# Sort platforms?
sort_cmd="cat"

# Associative array containing all results as keys
# for each key the value is the platform associated with the result
# e.g.
# [https://gcc.gnu.org/ml/gcc-testresults/2015-05/msg00072.html] = armv5tejl-unknown-linux-gnueabi
declare -A result_platform
# Associative array containing all results as keys
# for each key the value is the version of gcc associated with the result
# e.g.
# [https://gcc.gnu.org/ml/gcc-testresults/2015-05/msg00072.html] = 5.1.0
declare -A result_version
# array of all platforms.
declare -a platforms

# For the mail generation keep track of number of results added pr. platform
# pr. gcc version
# The key will be platform,gcc_version with the value keeping track of how
# many results we've added
declare -A mail_platform_version

# No dryrun by default
dryrun=0

print_usage()
{
  echo "Usage: $(basename $0) -b <buildstat> -r <results> -o <newbuildstat> -m <mailbody> [ -s ] [ -d ]"
  echo
  echo "-b buildstat.html file from the gcc website"
  echo "-r output from gcc-summary.sh"
  echo "-o the updated buildstat file produced by the script"
  echo "-m the mail body produced by the script ready for gcc-patches"
  echo "-s sort platforms (can be distruptive)"
  echo "-d dryrun, just show what would be done"
  echo
}

# Parse a buildstat result file
parse_results()
{
  local result ver platform url pfound p mailkey
  while read result
  do
    result=${result##*: } # Strip away markers from gcc-summary.sh output
    IFS="#"
    set -- $result
    unset IFS
    ver=$1
    platform=$2
    url=$3
    # Run through known platforms
    pfound=0
    for p in ${platforms[@]}; do
      [ $p = $platform ] && pfound=1 && break
    done
    [ $pfound -eq 0 ] && platforms+=($platform)
    if [ -n "${result_platform[$url]}" ]; then
      if [ "${result_platform[$url]}" = "$platform" ]; then
	echo "WARNING: ${ver}#${platform}#${url} already registered" >&2
      else
	echo "ERROR: $url already registered for platform ${result_platform[$url]}" >&2
      fi
    else
      # Result is unknown, add it to our data
      result_platform[$url]=$platform
      result_version[$url]=$ver
      echo "Adding ${ver}#${platform}#${url}" >&2
      mailkey="${platform},$ver"
      ((mail_platform_version[$mailkey]++))
    fi
  done < "$bsr"
}

# Parse a gcc buildstat.html file
parse_buildstat()
{
  local startplatform=0 inplatform=0 foundplatform=0 success=0
  local line bsplatform resurl resver
  # Start going through buildstat and see if we can find $platform
  while read line
  do
    if [ "$startplatform" -eq 1 ]; then
      # Extract platform and see if it matches
      bsplatform="$(echo $line | sed -e 's/<td>\(.*\)<\/td>/\1/')"
      platforms+=($bsplatform)
      startplatform=0
      foundplatform=1
    fi
    if [ $foundplatform -eq 1 ]; then
      case ${line:0:7} in
	"<a href")
		# Found a result line, parse it into our arrays
		resurl=$(echo "$line" | sed -n 's|.*"\([^"]*\)">.*|\1|p')
		resver=$(echo "$line" | sed -n 's|.*>\([0-9].*\)<.*|\1|p')
		result_platform[$resurl]=$bsplatform
		result_version[$resurl]=$resver
		;;
	*)
		;;
      esac
    fi
    if [ "${line:0:4}" = "<tr>" ]; then
      inplatform=1
      startplatform=1
    fi
    if [ "${line:0:5}" = "</tr>" ]; then
      inplatform=0
      startplatform=0
      foundplatform=0
    fi
  done < "$bs"
}

#printf "%s\n" ${platforms[@]}
#exit 1
# For each platform find all results and their associated versions
simple_format()
{
  local p resurl
  for p in ${platforms[@]};
  do
    echo $p
    for resurl in ${!result_platform[@]};
    do
      if [ ${result_platform[$resurl]} = $p ]; then
	echo "$resurl ${result_version[$resurl]}"
      fi
    done
  done
}

html_format()
{
  local tempfile p resurl ver numres comma hastable
  tempfile=$(mktemp)
  # Get the header from the existing buildstat
  if grep -q '<table>' $bs; then
    # There is a table already
    sed -n '1,/^<table>/p' $bs
    hastable=1
  else
    # No table, match the body tag instead
    sed -n '/^<\/body>/!p;//q' $bs
    hastable=0
    echo "<table>"
  fi

  for p in $(printf "%s\n" ${platforms[@]} | $sort_cmd);
  do
    :> $tempfile
    comma=","
    echo
    echo "<tr>"
    echo "<td>$p</td>"
    echo "<td>&nbsp;</td>"
    echo "<td>Test results:"
    for resurl in ${!result_platform[@]};
    do
      if [ ${result_platform[$resurl]} = $p ]; then
	echo "$resurl ${result_version[$resurl]}" >> $tempfile
      fi
    done
    # No sort and format data in the tempfile
    numres=$(wc -l < $tempfile)
    while read resurl ver
    do
      if [ $numres -eq 1 ]; then
	comma=""
      fi
      numres=$((numres-1))
      echo "<a href=\"$resurl\">$ver</a>$comma"
    done < <(sort -r -k2 $tempfile)
    echo "</td>"
    echo "</tr>"
  done
  echo
  # Add the old footer
  if [ $hastable -eq 0 ]; then
    echo "</table>"
    echo
    echo "</body>"
    echo "</html>"
  else
    sed -n '/^<\/table>/,$p' $bs
  fi
  rm -f $tempfile
}

mail_format()
{
  local key p ver v numstring
  declare -A versions
  # This is horrible, we run through the mail_platform_version array
  # once for each platform version and once just to find all the versions
  for key in ${!mail_platform_version[@]}
  do
    IFS=","
    set -- $key
    unset IFS
    p=$1
    ver=$2
    versions[$ver]=1
  done
  for v in $(echo ${!versions[@]} | xargs -n1 echo | sort -r);
  do
    echo "Testresults for $v:"
    for key in ${!mail_platform_version[@]}
    do
      IFS=","
      set -- $key
      unset IFS
      p=$1
      ver=$2
      [ $v != $ver ] && continue
      if [ ${mail_platform_version[$key]} -gt 1 ]; then
        numstring=" (${mail_platform_version[$key]})"
      else
	numstring=""
      fi
      echo "  $p$numstring"
    done | sort
    echo
  done
}

# Parse options
if [ $# -gt 0 ]; then
    while getopts b:r:o:m:sd opt
    do
        case $opt in
            b)
                bs=$OPTARG
                ;;
            r)
                bsr=$OPTARG
                ;;
            o)
                output=$OPTARG
                ;;
            m)
                mail=$OPTARG
                ;;
	    s)
		sort_cmd="sort"
		;;
	    d)
		dryrun=1
		;;
            \?)
                print_usage && exit 0
                ;;
        esac
    done
    shift `expr $OPTIND - 1`
else
    print_usage && exit 0
fi

# Check arguments
[ -z "$bs" ] && print_usage && exit 1
[ -z "$bsr" ] && print_usage && exit 2
[ -z "$output" ] && print_usage && exit 3
[ -z "$mail" ] && print_usage && exit 4
[ ! -r "$bs" ] && echo "FATAL: could not read $bs" && exit 1
[ ! -r "$bsr" ] && echo "FATAL: could not read $bsr" && exit 2

# Looks like we at least got arguments and readable files
# so let's go!
parse_buildstat
parse_results
if [ $dryrun -eq 0 ]; then
  html_format 1> $output
  mail_format > $mail
fi
